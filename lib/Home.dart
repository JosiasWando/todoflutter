import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List _todoList = [];
  Map<String, dynamic> _lastRemoved = Map();
  TextEditingController _workDialogController = TextEditingController();

  Future<File> _getFile() async {
    final dir = await getApplicationDocumentsDirectory();
    return File("${dir.path}/work_data.json");
  }

  _saveFile() async {
    var file = await _getFile();
    file.writeAsString(json.encode(_todoList));
  }

  _readFile() async {
    try {
      final file = await _getFile();
      return file.readAsString();
    } catch (e) {
      return null;
    }
  }

  Widget _createCheckBoxWork(context, index) {
    final item = _todoList[index]["title"] +
        DateTime.now().millisecondsSinceEpoch.toString();
    return Dismissible(
        key: Key(item),
        direction: DismissDirection.endToStart,
        background: Container(
          padding: EdgeInsets.all(16),
          color: Colors.red,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Icon(
                Icons.delete_forever,
                color: Colors.white,
              )
            ],
          ),
        ),
        onDismissed: (direction) {
          _lastRemoved = _todoList[index];
          setState(() {
            _todoList.removeAt(index);
          });
          _saveFile();

          final snack = SnackBar(
            duration: Duration(seconds: 5),
            content: Text("Work Delete"),
            action: SnackBarAction(
              label: "Undo",
              onPressed: () {
                setState(() {
                  _todoList.insert(index, _lastRemoved);
                });
                _lastRemoved = null;
                _saveFile();
              },
            ),
          );

          Scaffold.of(context).showSnackBar(snack);
        },
        child: CheckboxListTile(
          title: Text(
            _todoList[index]['title'],
            style: TextStyle(fontSize: 20),
          ),
          value: _todoList[index]['complete'],
          onChanged: (value) {
            setState(() {
              _todoList[index]['complete'] = value;
            });
            _saveFile();
          },
        ));
  }

  @override
  void initState() {
    super.initState();
    _readFile().then((data) {
      setState(() {
        _todoList = json.decode(data);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Todo List for Today"),
          backgroundColor: Colors.purple,
        ),
        body: Column(
          children: <Widget>[
            Expanded(
                child: ListView.builder(
                    itemCount: _todoList.length,
                    itemBuilder: _createCheckBoxWork))
          ],
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.purple,
          foregroundColor: Colors.white,
          elevation: 6,
          child: Icon(Icons.add),
          onPressed: () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text("Key o new Work"),
                    content: TextField(
                      decoration: InputDecoration(
                          labelText: "Key the name of new work"),
                      onChanged: (text) {},
                      controller: _workDialogController,
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("Cancel"),
                        onPressed: () {
                          _workDialogController.text = "";
                          Navigator.pop(context);
                        },
                      ),
                      FlatButton(
                        child: Text("Add"),
                        onPressed: () {
                          var workName = _workDialogController.text;
                          Map<String, dynamic> work = Map();
                          work["title"] = workName;
                          work["complete"] = false;
                          setState(() {
                            _todoList.add(work);
                            _workDialogController.text = "";
                          });
                          _saveFile();
                          Navigator.pop(context);
                        },
                      )
                    ],
                  );
                });
          },
        ));
  }
}
